import requests
import json
import logging
import time
import random
from CommentParser import CommentParser
import csv
from langdetect import detect
from datetime import datetime
from fake_useragent import UserAgent

"""
# If you would like to crawl GitHub contact us at support@github.com.
# We also provide an extensive API: https://developer.github.com/

User-agent: baidu
crawl-delay: 1


User-agent: *

Disallow: */pulse
Disallow: */tree/
Disallow: */blob/
Disallow: */wiki/
Disallow: /gist/
Disallow: */forks
Disallow: */stars
Disallow: */download
Disallow: */revisions
Disallow: */issues/new
Disallow: */issues/search
Disallow: */commits/
Disallow: */commits/*?author
Disallow: */commits/*?path
Disallow: */branches
Disallow: */tags
Disallow: */contributors
Disallow: */comments
Disallow: */stargazers
Disallow: */archive/
Disallow: */followers
Disallow: */following
Disallow: */blame/
Disallow: */watchers
Disallow: */network
Disallow: */graphs
Disallow: */raw/
Disallow: */compare/
Disallow: */cache/
Disallow: /.git/
Disallow: */.git/
Disallow: /*.git$
Disallow: /search/advanced
Disallow: /search
Disallow: */search
Disallow: /*q=
Disallow: /*.atom

Disallow: /ekansa/Open-Context-Data
Disallow: /ekansa/opencontext-*
Disallow: */tarball/
Disallow: */zipball/

Disallow: /account-login
Disallow: /Explodingstuff/
"""

class Scrapper:
    SEARCH_URL = 'https://api.github.com/search/repositories?q=+language:objective-c&sort=stars&order={0}&page={1}'
    GET_CONTENTS_URL = 'https://api.github.com/repos/{0}/contents/'
    RATE_LIMIT_MESSAGE = 'API rate limit exceeded'
    FAILURE_COUNT_LIMIT = 20
    USER_AGENT = UserAgent()

    NATURAL_LANGUAGE_ID = 'NaturalLanguageID'
    PROGRAMMING_LANGUAGE_NAME = 'ProgrammingLanguageName'
    REPO_ID = 'RepoID'
    SOURCE_ID = 'SourceID'
    COMMENT_ID = 'CommentID'
    COMMENT = 'Comment'
    COMMENT_TYPE = 'CommentType'
    NUMBER_OF_LINES = 'NumberOfLines'
    DOWNLOAD_URL = 'DownloadUrl'

    def __init__(self, out_file, comments_limit, file_extension, programming_language):
        self.out_file = out_file
        self.comments_limit = comments_limit
        self.file_extension = file_extension
        self.programming_language = programming_language
        self.comments_parser = CommentParser()
        self.page = -1
        self.failure_count = 0
        self.comments_count = 0
        self.limited_addresses = {}

    def log_with_timestamp(self, message):
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        timestamped_message = current_time + ',' + message
        logging.debug(timestamped_message)

    def http_reliable_get_request(self, url):
        """
        Executes https request reliably

        # Arguments:
            url: url for https request

        # Returns None if we fail more than Scrapper.FAILURE_COUNT_LIMIT
        """

        for i in range(20):
            try:
                self.log_with_timestamp('Starting request from:{0} '.format(url))
                default_headers = {'User-Agent' : Scrapper.USER_AGENT.random}
                response = requests.get(url, headers=default_headers, timeout=15)
                self.log_with_timestamp('Finished request from:{0} '.format(url))
                return response.text
            except Exception as e:
                self.log_with_timestamp(str(e))
                raise RuntimeError('Request to the website failed')

        return None

    def get_repos(self, url):
        """
        Gets repos from github using search api. This will return one page.

        # Arguments:
            url: url from which we get repos

        # Returns list of repos or None if we are unable to process this request after some time
        """
        response = self.http_reliable_get_request(url)

        if response is None:
            return None

        full_names = []

        try:
            full_names = [item['full_name'] for item in json.loads(response)['items']]
        except Exception as e:
            self.log_with_timestamp("Couldn't proccess response from:{0}".format(url))
            self.log_with_timestamp('Response is:{0}'.format(response))
            self.log_with_timestamp('Current exception:{0}'.format(e))
        
        sleep_time = random.randint(1,3)
        time.sleep(sleep_time)
        
        return full_names

    def get_file_list(self, url, file_extension, repo):
        """
        Gets a list with download_url for all files with 'file_extension' from repo at 'url'.

        # Arguments:
            url: url from which to get file list
            file_extension: file type extension

        # Returns an empty list if we are unable to process request.
        """
        file_list = []
        response = self.http_reliable_get_request(url)

        if response is None:
            return []

        response_json = json.loads(response)

        sleep_time = random.randint(1,4)
        time.sleep(sleep_time)

        file_list = []
        dir_list = []

        try:
            for i in range(len(response_json)):
                item = response_json[i]

                if item['type'] == 'file' and item['name'].endswith(file_extension):
                    file_list.append(item['download_url'])
                    self.get_file_content_and_write_comments_to_file(item['download_url'], repo)
                elif item['type'] == 'dir':
                    dir_list.append(item['name'])

            for dir_name in dir_list:
                new_url = url + dir_name + '/' if url.endswith('/') else url + '/' + dir_name + '/'

                if 'src' in new_url or 'test' in new_url:
                    file_list += self.get_file_list(new_url, file_extension, repo)
        except Exception as e:
            self.log_with_timestamp('Unable to parse response from:{0}'.format(url))
            self.log_with_timestamp('Current exception:{0}'.format(e.args[0]))

        return file_list

    def get_file_content_and_write_comments_to_file(self, url, repo):

        # Get file contents from github
        file_content = self.http_reliable_get_request(url)

        if file_content is None:
            return None

        # Instantiate our CommentParser class and parse the comments out of file
        cp = CommentParser()
        comments = cp.parse_comments(file_content)

        self.comments_count += len(comments)

        for line, comment in comments.items():
            language = 'unknown'
            
            try:
                # Predict language of text
                language = self.detect(comment)
            except Exception as e:
                language = 'unknown'

            file_name = url.replace('https://raw.githubusercontent.com','')
            
            comment_data_row_dict = {
                Scrapper.NATURAL_LANGUAGE_ID : language,
                Scrapper.PROGRAMMING_LANGUAGE_NAME : self.programming_language,
                Scrapper.REPO_ID : repo, 
                Scrapper.SOURCE_ID : file_name,
                Scrapper.COMMENT_ID : repo + '/' + file_name + '/' + str(line),
                Scrapper.COMMENT : comment,
                Scrapper.COMMENT_TYPE : ''
                }
            
            number_of_lines = len(file_content.split('\n'))

            comment_source_data_row_dict = {
                Scrapper.REPO_ID : repo,
                Scrapper.SOURCE_ID : file_name,
                Scrapper.NUMBER_OF_LINES : number_of_lines,
                Scrapper.DOWNLOAD_URL : url
            }

            self.log_with_timestamp('Writing to comments source file')
            self.write_comment(comment_data_row_dict, comment_source_data_row_dict)
            self.log_with_timestamp('Wrote to comments source file')

    def write_comment(self, comment_data_dict, comment_source_dict):
        """
        Write actual comment to csv file
        """

        with open(self.out_file + '.csv', 'a', encoding='utf-8') as outcsv:
            fieldnames = [Scrapper.NATURAL_LANGUAGE_ID, Scrapper.PROGRAMMING_LANGUAGE_NAME, Scrapper.REPO_ID, Scrapper.SOURCE_ID, Scrapper.COMMENT_ID, Scrapper.COMMENT, Scrapper.COMMENT_TYPE]
            writer = csv.DictWriter(outcsv, fieldnames = fieldnames, delimiter='\t')

            writer.writerow(comment_data_dict)

        with open(self.out_file + '_2.csv', 'a', encoding='utf-8') as outcsv_source:
            fieldnames = [Scrapper.REPO_ID, Scrapper.SOURCE_ID, Scrapper.NUMBER_OF_LINES, Scrapper.DOWNLOAD_URL]
            file_source_writer = csv.DictWriter(outcsv_source, fieldnames = fieldnames, delimiter='\t')

            file_source_writer.writerow(comment_source_dict)


    def run(self):

        # Write template header to first csv file
        with open(self.out_file + '.csv', 'w', encoding='utf-8') as outcsv:
            fieldnames = [Scrapper.NATURAL_LANGUAGE_ID, Scrapper.PROGRAMMING_LANGUAGE_NAME, Scrapper.REPO_ID, Scrapper.SOURCE_ID, Scrapper.COMMENT_ID, Scrapper.COMMENT, Scrapper.COMMENT_TYPE]

            writer = csv.DictWriter(outcsv, fieldnames = fieldnames, delimiter='\t')
            writer.writeheader()

        # Write filtered header to second csv file
        with open(self.out_file + '_2.csv', 'w', encoding='utf-8') as outcsv_source:
            fieldnames = [Scrapper.REPO_ID, Scrapper.SOURCE_ID, Scrapper.NUMBER_OF_LINES, Scrapper.DOWNLOAD_URL]

            file_source_writer = csv.DictWriter(outcsv_source, fieldnames = fieldnames, delimiter='\t')
            file_source_writer.writeheader()

        # Get comments_limit(10.000) comments and write them to csv files
        while self.comments_count <= self.comments_limit or self.page < 35:
            self.page += 1

            repos_desc =self.get_repos(Scrapper.SEARCH_URL.format('desc', self.page))
            repos_desc = repos_desc if repos_desc is not None else []

            repos = repos_desc

            if repos is None or len(repos) == 0:
                self.log_with_timestamp("No repos fetched.")
                continue

            for repo in repos:
                url = Scrapper.GET_CONTENTS_URL.format(repo)
                self.get_file_list(url, self.file_extension, repo)


logging.getLogger().setLevel(logging.DEBUG)
s = Scrapper('comments', 10000, '.m', 'objective-c')
s.run()

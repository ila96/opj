import csv


def fix_comments_source_location():
    tsv_file = open("comments_source_location.tsv", "r")
    new_tsv_file = open("comments_source_location2.tsv", "w")

    seen = set()

    for line in tsv_file:
        if line not in seen:
            seen.add(line)
            new_tsv_file.write(line)
    tsv_file.close()
    new_tsv_file.close()


def fix_comments():
    tsv_file = open("comments_before_preprocessing.tsv", "r", encoding="utf-8")
    read_tsv = csv.reader(tsv_file, delimiter="\t")
    new_tsv_file = open("comments_before_preprocessing2.tsv", "w", encoding="utf-8")

    for row in read_tsv:
        comment = row[5]
        comment = comment.replace("\n", "\\n")
        line = f"{row[0]}\t{row[1]}\t{row[2]}\t{row[3]}\t{row[4]}\t{comment}\n"
        new_tsv_file.write(line)

    tsv_file.close()
    new_tsv_file.close()


# fix_comments_source_location()
fix_comments()

import os
import re
from CommentParser import CommentParser

"""
with open('testFile.ts', 'r') as file:
    command = file.read()
    #regex = '//(.*){2,}\n[ \t]+[a-zA-Z]'
    #info = re.findall(regex, command)
    #print(info)
    info = re.findall('//(.*)\n', command, re.DOTALL)
    #print(info)
    #for cp in info:
        #print(cp)
    for match in re.finditer('//(.*?)\n', command):
        for idx in range(0,4):
            print(match.span(idx), match.group(idx))
"""
"""
cp = CommentParser()

with open('testFile.ts', 'r') as file:
    command = file.readlines()
    comments = cp.parse_comments(command)

    print(comments)
"""

def f1():
    raise Exception('das ist sehr gut.')

try:
    f1()
except Exception as e:
    print(e.message)
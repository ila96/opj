import os
import numpy as np
import pandas as pd
import re
import nltk
from nltk.tokenize import word_tokenize
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import preprocessing
from sklearn.utils.class_weight import compute_class_weight
from sklearn.pipeline import Pipeline
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_validate
from sklearn.metrics import confusion_matrix
from joblib import dump, load
import pickle
import argparse
import json

def remove_stop_words(comments):
    """ Expects list of list of words """
    """ Returns a list of list of words """
    nltk.download('stopwords')
    from nltk.corpus import stopwords

    result = []

    for comm in comments:
        comm_array = comm.split()
        new_comment = []
        for word in comm_array:
            if word not in stopwords.words('english'):
                new_comment.append(word)

        result.append(' '.join(new_comment))

    return result

def remove_non_alpahbetic_characters(comments):
    """ Content is numpy array"""
    result = []

    for comment in comments:
        # replace non-alphabetical characters with space
        comm = re.sub("[^A-Za-z0-9]", ' ', comment)
        # replace ' with empty space
        comm = re.sub("'", '', comm)
        # replace more than one space wit just one blank
        comm = re.sub('[ \\n\\t\\r]{2,}', ' ', comm)
        # trim ends
        comm = comm.strip()
        result.append(comm)

    return result


def normalize_words(comments):
    """ make words lower case """
    result = []
    
    for comment in comments:
        new_comment = comment.lower()
        result.append(new_comment)

    print("normalize_words accepts:{0} and returns type:{1}".format(type(comments), type(result)))
    return result

def preprocess_comments(comments):
    """ Accepts python list or numpy array and returns the same type"""
    result = remove_non_alpahbetic_characters(comments)

    return result

def merge_functional(labels):
    new_labels = []

    for label in labels:
        if 'functional' in label:
            new_labels.append('functional')
        else:
            new_labels.append(label)

    return new_labels

def write_to_file(filename, data):
    with open(filename, 'a+', encoding='utf8') as f:
            for st in data:
                f.write(st + '\n')

def log_model(filename, model_name, train_scores, test_scores, confusion_matrix, report, label_encoder):
    with open(filename, 'a+', encoding='utf16') as f:
        f.write("# PARAMETERS\n\n")
        f.write(model_name + '\n' + '\n')
        f.write("# ACCURACY RESULT\n\n")
        train_message = "{0:.2f} mean train accuracy with a standard deviation of {1:.2f}".format(train_scores.mean(), train_scores.std())
        f.write(train_message + '\n')
        test_message = "{0:.2f} mean test accuracy with a standard deviation of {1:.2f}".format(test_scores.mean(), test_scores.std())
        f.write(test_message + '\n' +'\n')
        # f.write("# REPORT DETAILS\n\n")
        # f.write(report + '\n' + '\n')
        # f.write(np.array2string(report, separator=', '))
        f.write("# CONFUSION MATRIX\n\n")
        le_name_mapping = dict(zip(label_encoder.classes_, label_encoder.transform(label_encoder.classes_)))
        f.write(str(le_name_mapping))
        f.write('\n')
        f.write(np.array2string(confusion_matrix, separator=', '))
        f.write('\n\n===============================================\n\n')
        

def driver(args):
    ### load file
    df = pd.read_csv(args.input, sep='\t')
    filtered = df[df['NaturalLanguageID'] != 'unknown']
    filtered = filtered.dropna()
    filtered['NormalizedCommentType'] = filtered['CommentType'].str.lower()
    filtered = filtered[filtered['NormalizedCommentType'] != 'note']

    if args.drop_ide is not None and args.drop_ide:
        filtered = filtered[filtered['NormalizedCommentType'] != 'ide']

    ### shuffle data
    filtered = filtered.sample(frac = 1)

    ### extract last two columns as they are important
    comments = filtered['Comment'].tolist()# to numpy
    labels = filtered['NormalizedCommentType'].tolist()
    assert(len(comments) == len(labels))

    ### Preprocess
    data = comments
    data = remove_non_alpahbetic_characters(data)

    if args.normalize:
        data = normalize_words(data)

    if args.remove_stop_words:
        data = remove_stop_words(data)

    should_merge_functional = args.merge_functional

    ### Merge functional comments
    if should_merge_functional:
        print('Merging functional commments')
        labels = merge_functional(labels)

    ### Input type
    ngram_range = 1
    if args.ngram == 'unigram':
        ngram_range = 1
    elif args.ngram == 'bigram':
        ngram_range = 2
    elif args.ngram == 'trigram':
        ngram_range = 3

    ### Minor note: If we use ngram_range=(1, ngram_range) and e.g ngram_range = 2 then we will use both unigrams and bigrams
    ### TODO: Implement IDF
    ### tf idf
    if args.feature_type == 'tf_idf':
        if args.model == 'bernoulli_naive_bayes':
            print('Not able to use tfidf with Bernoulli Naive Bayes')
            return
        
        vectorizer = TfidfVectorizer(ngram_range=(1, ngram_range), max_df=args.max_df, min_df=args.min_df)
    else:
        vectorizer = CountVectorizer(ngram_range=(1, ngram_range), binary=args.model == 'bernoulli_naive_bayes')

    ### encode labels
    le = preprocessing.LabelEncoder()
    y = le.fit_transform(labels)

    if args.use_class_weight:
        cw = compute_class_weight('balanced', np.unique(y), y=y)
        class_weight = dict(enumerate(cw.flatten(), 0))
    else:
        class_weight = None

    ### model type and penalty
    model_type = args.model
    penalty = args.penalty
    model = None

    if 'bayes' in model_type:
        model = algorithms[model_type]() ### this means that if input count is > 0.1 then we treat the feature as present
    elif model_type == 'svm':
        # SVM will use one vs rest tactics
        if args.penalty == 'l1':
            print("SVM with L1 penalty loss is not supported")
            return

        model = algorithms[model_type](penalty=penalty, loss=args.loss, class_weight=class_weight, max_iter=1500)
    else:
        # for logistic regression we want to use cross-entropy loss
        # SVM will use one vs rest tactics
        model = algorithms[model_type](penalty=penalty,  loss='log', class_weight=class_weight, early_stopping=True)

    ### Implement K-fold validation
    data = np.array(data)
    y = np.array(y)

    accuracy = []
    recall = []

    # fit vectorizer to dataset
    vectorizer.fit(data) # this will create vocabulary based on words in data
    features = vectorizer.transform(data)

    ### train using 10 layer stratified K fold
    skf =  StratifiedKFold(n_splits=10)
    #scores = cross_val_score(model, features, y, cv=skf)
    results = cross_validate(model, features,  y, scoring='accuracy', cv=skf, return_train_score=True, return_estimator=True)
    print("%0.2f mean train accuracy with a standard deviation of %0.2f" % (results['train_score'].mean(), results['train_score'].std()))
    print("%0.2f mean test accuracy with a standard deviation of %0.2f" % (results['test_score'].mean(), results['test_score'].std()))

    ### get best estimator
    index = np.argmax(results['test_score'])
    best_estimator = results['estimator'][index]

    ### log model stats
    model_name = "normalize={0}\nremove_stop_words={1}\nuse_class_weight={2}\nmerge_functional={3}\nngram={4}\nmodel={5}\npenalty={6}\nfeature_type={7}\nmax_df={8}\nmin_df={9}\ndrop_ide={10}".format(args.normalize, args.remove_stop_words, args.use_class_weight, args.merge_functional, args.ngram, args.model, args.penalty, args.feature_type, args.max_df, args.min_df, args.drop_ide)
    model_path = os.path.join(args.output, model_name)

    ###
    preds = best_estimator.predict(features)
    cm = confusion_matrix(y, preds)
    cr = classification_report(y, preds)
    
    log_model(os.path.join(args.output, 'metrics.txt'), model_name, results['train_score'], results['test_score'], cm, cr, le)

    ### save model
    # dump(best_estimator, model_path)
    
    """
    Load model
    clf = load('filename.joblib')
    """

preprocess_map = {'remove_non_alphabetical':remove_non_alpahbetic_characters, 'normalize':normalize_words, 'stopwords':remove_stop_words}

# 'bag_of_words', 'bigram', 'trigram'

algorithms = {'multinomial_naive_bayes':MultinomialNB, 'bernoulli_naive_bayes': BernoulliNB, 'logistic_regression': SGDClassifier, 'svm':LinearSVC}

def main():
    parser = argparse.ArgumentParser("Train parser")

    parser.add_argument('-i','--input', required=True,
        help='Path to the file with training data. This should be .tsv file. The file should contain both features and labels')
    parser.add_argument('-o', '--output', required=False, default='',
        help='Output directory for trained model')
    parser.add_argument('--normalize', required=False, default=False, type=bool,
        help='Whether or not to normalize words to lowercase letters')
    parser.add_argument('--remove_stop_words', required=False, default=False, type=bool,
        help='Whether or not to normalize words to lowercase letters')
    parser.add_argument('--use_class_weight', required=False, default=True, type=bool, help='Whether or not to use class weight. This is useful in cases when we have imbalanced classes')
    parser.add_argument('--merge_functional', required=False, default=False, type=bool, help='Whether or not to merge functional comments')
    parser.add_argument('--ngram', required=True, choices=['unigram', 'bigram', 'trigram'], help='Which model to of ngram to use')
    parser.add_argument('--model', required=True, choices=['multinomial_naive_bayes', 'bernoulli_naive_bayes', 'logistic_regression','svm'], help='Which model to use for train')
    parser.add_argument('--penalty', required=True, choices=['l1', 'l2'], help='Which penalty to use ')
    parser.add_argument('--feature_type', required=True, choices=['tf', 'tf_idf'], help='Which feature type to use')
    parser.add_argument('--max_df', required=False, default='1.0', type=float, help='Maximum DF limit for words. By default it is 1.0. This must be fraction of documents')
    parser.add_argument('--min_df', required=False, default='0.0', type=float,  help='Minimum DF limit for words. This is percentage, 0.0 means don\'t filter')
    parser.add_argument('--drop_ide', required=False, default=False, type=bool,  help='Whether or not to drop comments with IDE label. This is needed because we have just 3 samples of IDE comments, so it can\'t be used with stratified kfold')
    parser.add_argument('--loss', required=False, default=False, type=bool,  help='Whether to use L1 or L2 loss function for SVM')
    parser.set_defaults(func=driver)

    args = parser.parse_args()
    args.func(args)

if __name__ == '__main__':
    main()
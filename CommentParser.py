from enum import Enum


class State(Enum):
    start = 1
    single_line = 2
    multi_line = 3


class CommentParser():
    MULTI_LINE_COMMENTS_START = '/*'
    MULTI_LINE_COMMENTS_END = '*/'
    SINGLE_LINE = '//'

    def __init__(self):
        self.state = State.start
        self.comment = ''
        self.line_number = ''
        self.comments = {}
        self.actions = {State.start : self.start_action, State.single_line : self.single_line_action, State.multi_line : self.multi_line_action}
        self.multi_line_splitter = '\\n'

    def start_action(self, current_line, current_line_number):
        """
        Action to move parser from starting state.

        # Arguments:
            current_line: content of the current line of file.
            current_line_number: number of current line.
        """
        if current_line.find(CommentParser.SINGLE_LINE) != -1:
            index = current_line.find(CommentParser.SINGLE_LINE)
            self.comment = current_line[index:].replace('\n', '').replace('\t', '\\t')
            self.line_number = current_line_number

            if not current_line.lstrip().startswith(CommentParser.SINGLE_LINE):
                self.go_to_start_state()
            else:
                self.state = State.single_line
        elif current_line.find(CommentParser.MULTI_LINE_COMMENTS_START) != -1:
            start_index = current_line.find(CommentParser.MULTI_LINE_COMMENTS_START)
            end_index = current_line.find(CommentParser.MULTI_LINE_COMMENTS_END)

            if end_index == -1:
                self.comment = current_line[start_index:].replace('\n', '').replace('\t', '\\t')
                self.line_number = current_line_number
                self.state = State.multi_line
            else:
                self.comment = current_line[start_index:end_index + len(CommentParser.MULTI_LINE_COMMENTS_END)]\
                    .replace('\n', '').replace('\t', '\\t')
                self.line_number = current_line_number
                self.go_to_start_state()

    def single_line_action(self, current_line, current_line_number):
        """
        Action for parsing single line comments.

        # Arguments:
            current_line: content of the current line of file.
            current_line_number: number of current line.
        """
        if current_line.lstrip().startswith(CommentParser.SINGLE_LINE):
            self.comment += self.multi_line_splitter + current_line.lstrip().replace('\n', '').replace('\t', '\\t')
        else:
            # save comment and line
            self.go_to_start_state()

    def multi_line_action(self, current_line, current_line_number):
        """
        Action for parsing multi line comments.

        # Arguments:
            current_line: content of the current line of file.
            current_line_number: number of current line.
        """
        index = current_line.find(CommentParser.MULTI_LINE_COMMENTS_END)
        
        if index == -1:
            self.comment += self.multi_line_splitter + current_line.lstrip().replace('\n', '').replace('\t', '\\t')
        else:
            self.comment += self.multi_line_splitter + \
                            current_line[:index + len(CommentParser.MULTI_LINE_COMMENTS_END)]\
                                .replace('\n', '').replace('\t', '\\t')
            self.go_to_start_state()

    def go_to_start_state(self):
        """
        Moves parser to starting state.
        """
        self.comments[self.line_number] = self.comment
        self.comment = ''
        self.line_number = ''
        self.state = State.start

    def __reset__(self):
        """
        Prepares parser for file parsing.
        """
        self.comments = {}
        self.go_to_start_state()

    def parse_comments(self, file_content):
        """
        Parses comments and return them as dictionary where key is line and value is comment on that line.
        # Arguments:
            file_content: Either list of strings or string representing file content. If passing list of strings then that list should represent one file.
        """
        self.state = State.start
        current_line_number = 0

        if type(file_content) == type('a'):
            file_content = file_content.split('\n')
            self.multi_line_splitter = '\\n'

        for line in file_content:
            current_line_number += 1

            action = self.actions[self.state]
            action(line, current_line_number)
        
        return self.comments

    def parse_comments_from_path(self, file_path):
        """
        Parses comments and return them as dictionary where key is line and value is comment on that line.
        # Arguments:
            file_path: path to the file that should be parsed
        """
        with open(file_path, 'r', encoding='utf-8') as file:
            comments = self.parse_comments(file)

        return comments

import os

import models

# normalize_choices = [False, True]
# remove_stop_words_choices = [False, True]
# use_class_weight_choices = [False, True]
# merge_functional_choices = [False, True]
# ngram_choices = [False, True]
# model_choices = ['multinomial_naive_bayes', 'bernoulli_naive_bayes', 'logistic_regression','svm']
# penalty_choices = ['l1', 'l2']
# feature_type_choices = ['tf', 'tf_idf']
# max_df_choices = [0.8, 0.9, 1.0]
# min_df_choices = [0.0, 0.1, 0.2]
# loss_choices = ['hinge', 'squared_hinge']

"""
    [1]
        - max_df, min_df, tf (default), tf_idf , remove_stop_words
    [2]
        - max_df, min_df, tf (default), tf_idf , remove_stop_words
    [3]
        - max_df, min_df, tf (default), tf_idf , remove_stop_words  + penalty_choices
    [4]
        - max_df, min_df, tf (default), tf_idf , remove_stop_words  + (penalty fixed) , loss_choices
"""


class Args:
    def __init__(self, **kwds):
        self.__dict__.update(kwds)


if os.path.exists("models/metrics.txt"):
    os.remove("models/metrics.txt")

# Multinomial naive bayes
args = Args(input='comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight=False, merge_functional=False, ngram=False, model='multinomial_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input='comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight=False, merge_functional=True, ngram=False, model='multinomial_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = True, use_class_weight = False, merge_functional = False, ngram = False, model = 'multinomial_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = True, use_class_weight = False, merge_functional = True, ngram = False, model = 'multinomial_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'multinomial_naive_bayes', penalty = 'l1', feature_type = 'tf_idf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'multinomial_naive_bayes', penalty = 'l1', feature_type = 'tf_idf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'multinomial_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 0.7, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'multinomial_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 0.7, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'multinomial_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.3, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'multinomial_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.3, drop_ide = True, loss='hinge')
models.driver(args)
# Bernouli naive bayes
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'bernoulli_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'bernoulli_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = True, use_class_weight = False, merge_functional = False, ngram = False, model = 'bernoulli_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = True, use_class_weight = False, merge_functional = True, ngram = False, model = 'bernoulli_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'bernoulli_naive_bayes', penalty = 'l1', feature_type = 'tf_idf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'bernoulli_naive_bayes', penalty = 'l1', feature_type = 'tf_idf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'bernoulli_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 0.7, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'bernoulli_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 0.7, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'bernoulli_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.3, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'bernoulli_naive_bayes', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.3, drop_ide = True, loss='hinge')
models.driver(args)
# Logistic regression
# TODO: Some of this tests should be edited according report
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'logistic_regression', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'logistic_regression', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = True, use_class_weight = False, merge_functional = False, ngram = False, model = 'logistic_regression', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = True, use_class_weight = False, merge_functional = True, ngram = False, model = 'logistic_regression', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'logistic_regression', penalty = 'l1', feature_type = 'tf_idf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'logistic_regression', penalty = 'l1', feature_type = 'tf_idf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'logistic_regression', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.3, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'logistic_regression', penalty = 'l1', feature_type = 'tf', max_df = 1.0, min_df = 0.3, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'logistic_regression', penalty = 'l1', feature_type = 'tf', max_df = 0.7, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'logistic_regression', penalty = 'l1', feature_type = 'tf', max_df = 0.7, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'logistic_regression', penalty = 'l2', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'logistic_regression', penalty = 'l2', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
# SVM
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = True, use_class_weight = False, merge_functional = False, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = True, use_class_weight = False, merge_functional = True, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf_idf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf_idf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf', max_df = 0.7, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf', max_df = 0.7, min_df = 0.0, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf', max_df = 1.0, min_df = 0.3, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf', max_df = 1.0, min_df = 0.3, drop_ide = True, loss='hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = False, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='squared_hinge')
models.driver(args)
args = Args(input = 'comments.tsv', output='models', normalize=False, remove_stop_words = False, use_class_weight = False, merge_functional = True, ngram = False, model = 'svm', penalty = 'l2', feature_type = 'tf', max_df = 1.0, min_df = 0.0, drop_ide = True, loss='squared_hinge')
models.driver(args)

import csv
import re

def parse_results():
    results = open('models/metrics.txt', 'r')
    output_csv = open('models/results.csv', mode='w', newline='')
    csv_writer = csv.writer(output_csv, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    lines = results.readlines()
    matrix_found = False
    delimiter = True
    for line in lines:
        if line.startswith('[['):
            matrix_found = True
        if matrix_found == True:
            nums = list(line[:-2].replace('[', '').replace(' ', '').replace(']','').split(','))
            csv_writer.writerow(nums)
        if line.endswith(']]\n'):
            matrix_found = False
            csv_writer.writerow("")
        if line[0].islower():
            params = list(line[:-1].split('='))
            csv_writer.writerow(params)
        if line.startswith('0') or line.startswith('1'):
            csv_writer.writerow(re.findall('\d*\.?\d+',line))
        if line.startswith('#'):
            csv_writer.writerow("")
        if "===============================================" in line:
            delimiter = not delimiter
            if delimiter:
                csv_writer.writerow("")
                csv_writer.writerow("============")
                csv_writer.writerow("")
            
    results.close()
    output_csv.close()
    return

parse_results()
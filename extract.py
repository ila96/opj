import os
from CommentParser import CommentParser
import csv
import urllib.parse
from langdetect import detect

NATURAL_LANGUAGE_ID = 'NaturalLanguageID'
PROGRAMMING_LANGUAGE_NAME = 'ProgrammingLanguageName'
REPO_ID = 'RepoID'
SOURCE_ID = 'SourceID'
COMMENT_ID = 'CommentID'
COMMENT = 'Comment'
COMMENT_TYPE = 'CommentType'
NUMBER_OF_LINES = 'NumberOfLines'
DOWNLOAD_URL = 'DownloadUrl'

# https://github.com/eliaskg/Hacky/blob/master/Hacky/NSDate%2BRelativeDate.h
SOURCE_URL_PATTERN = 'github.com/{0}/{1}/blob/{2}{3}'


def write_comment(comment_data_dict, comment_source_dict):
    if comment_data_dict:
        with open('comments.tsv', 'a', encoding='utf-8', newline='') as outcsv:
            fieldnames = [NATURAL_LANGUAGE_ID, PROGRAMMING_LANGUAGE_NAME, REPO_ID, SOURCE_ID, COMMENT_ID, COMMENT, COMMENT_TYPE]
            writer = csv.DictWriter(outcsv, fieldnames=fieldnames, delimiter='\t')

            writer.writerow(comment_data_dict)

    if comment_source_dict:
        with open('comments_source_location.tsv', 'a', encoding='utf-8', newline='') as outcsv_source:
            fieldnames = [REPO_ID, SOURCE_ID, NUMBER_OF_LINES, DOWNLOAD_URL]
            file_source_writer = csv.DictWriter(outcsv_source, fieldnames=fieldnames, delimiter='\t')

            file_source_writer.writerow(comment_source_dict)


def init_csv_files():
    if os.path.exists('comments.tsv') or os.path.exists('comments_source_location.tsv'):
        return

    with open('comments.tsv', 'w', encoding='utf-8', newline='') as outcsv:
        fieldnames = [NATURAL_LANGUAGE_ID, PROGRAMMING_LANGUAGE_NAME, REPO_ID, SOURCE_ID, COMMENT_ID, COMMENT, COMMENT_TYPE]

        writer = csv.DictWriter(outcsv, fieldnames = fieldnames, delimiter='\t')
        writer.writeheader()

    with open('comments_source_location.tsv', 'w', encoding='utf-8', newline='') as outcsv_source:
        fieldnames = [REPO_ID, SOURCE_ID, NUMBER_OF_LINES, DOWNLOAD_URL]

        file_source_writer = csv.DictWriter(outcsv_source, fieldnames=fieldnames, delimiter='\t')
        file_source_writer.writeheader()


def get_files(dir_path, file_extension):
    files = []

    for item_name in os.listdir(dir_path):
        current_item_path = os.path.join(dir_path, item_name)

        if item_name.endswith(file_extension):
            files.append(current_item_path)
        
        if os.path.isdir(current_item_path):
            files += get_files(current_item_path, file_extension)

    return files


def extract_comments_from_file(file_path):
    cp = CommentParser()
    return cp.parse_comments_from_path(file_path)


def predict_language(text):
    """
    Predicts language of text

    # Arguments:
        text: text for which this method returns language

    # Returns:
        string representing language of text.
    """
    try:
        return detect(text)
    except Exception as e:
        return 'unknown'


def driver(programming_language, file_extension):
    # Extract comments from both .h and .m
    init_csv_files()
    comment_count = 0

    for user in os.listdir('data'):
        
        for repo_dir in os.listdir(os.path.join('data', user)):

            if not os.path.isdir(os.path.join('data', user, repo_dir)):
                continue

            branch = 'master'
            repo = repo_dir 

            split = repo_dir.split('-')
            if len(split) == 2:
                repo = split[0]
                branch = split[1]

            repo_id = user + '/' + repo
            repo_root = os.path.join('data', user, repo_dir)
            files = get_files(repo_root, file_extension)
            
            for file_path in files:

                with open(file_path, 'r', encoding='utf-8') as file:
                    file_content = file.read()

                comments = extract_comments_from_file(file_path)
                comment_count += len(comments)
                
                # so path is like 'data\ipreencekmr\opentok-ios-sdk-samples-master'
                # and we end up with 'https://github.com/ipreencekmr/opentok-ios-sdk-samples-master/blob/master/Simple-Multiparty/Multi-Party-Call/libyuv/planar_functions.h
                # so just replace '-branch' with '' in url
                file_relative_path = file_path.replace(repo_root, '')
                file_relative_url = file_relative_path.replace('\\', '/')
                url = SOURCE_URL_PATTERN.format(user, repo, branch, file_relative_path)\
                    .replace('\\', '/').replace('-' + branch, '')
                url = 'https://' + urllib.parse.quote(url)

                for line, comment in comments.items():
                    language = predict_language(comment)

                    comment_data_row_dict = {
                            NATURAL_LANGUAGE_ID : language,
                            PROGRAMMING_LANGUAGE_NAME : programming_language,
                            REPO_ID: repo_id,
                            SOURCE_ID: file_relative_url,
                            COMMENT_ID: repo_id + '/' + file_relative_url.strip('/') + '/' + str(line),
                            COMMENT: comment,
                            COMMENT_TYPE: ''
                            }
                    write_comment(comment_data_row_dict, None)

                comment_source_data_row_dict = {
                    REPO_ID: repo_id,
                    SOURCE_ID: file_relative_url,
                    NUMBER_OF_LINES: len(file_content.split('\n')),
                    DOWNLOAD_URL: url
                }
                write_comment(None, comment_source_data_row_dict)

    print("Comments extracted:{0}".format(comment_count))


# get .h and .m files
# for each file extract comments
# write comments to file

driver('objective-c', '.m')
driver('objective-c', '.h')

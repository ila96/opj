import numpy as np
import pandas as pd
import models

normalize_choices = [False, True]
remove_stop_words_choices = [False, True]
use_class_weight_choices = [False, True]
merge_functional_choices = [False, True]
ngram_choices = [False, True]
model_choices = ['multinomial_naive_bayes', 'bernoulli_naive_bayes', 'logistic_regression','svm']
penalty_choices = ['l1', 'l2']
feature_type_choices = ['tf', 'tf_idf']
max_df_choices = [0.8, 0.9, 1.0]
min_df_choices = [0.0, 0.1, 0.2]
loss_choices = ['hinge', 'squared_hinge']

class Args:
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

for normalize in normalize_choices:
    for remove_stop_words in remove_stop_words_choices:
        for use_class_weight in use_class_weight_choices:
            for merge_functional in merge_functional_choices:
                for ngram in ngram_choices:
                    for model in model_choices:
                        for feature_type in feature_type_choices:
                            for max_df in max_df_choices:
                                for min_df in min_df_choices:
                                    # no need to specify loss and penalty loss for naive bayes classificators
                                    if 'bayes' in model:
                                        args = Args(input = 'comments.tsv', output='models',
                                                normalize=normalize, remove_stop_words = remove_stop_words, use_class_weight = use_class_weight, merge_functional = merge_functional, ngram = ngram, model = model, penalty = 'penalty', feature_type = feature_type, max_df = max_df, min_df = min_df, drop_ide = True, loss='loss')
                                        
                                        command = ".\models.py --input {0} --output {1} --normalize {2} --remove_stop_words {3} --use_class_weight {4} --merge_functional {5} --ngram {6} --model {7} --penalty {8} --feature_type {9} --max_df {10} --min_df {11} --drop_ide {12} --loss {13}".format('comments.tsv', 'models', normalize, remove_stop_words, use_class_weight, merge_functional, ngram, model, 'penalty', feature_type, max_df, min_df, True, 'loss')
                                        print("============================================================================")
                                        print(command)
                                        print("============================================================================")
                                        models.driver(args)
                                        continue

                                    for penalty in penalty_choices:

                                        if model != 'svm':
                                            args = Args(input = 'comments.tsv', output='models',
                                                    normalize=normalize, remove_stop_words = remove_stop_words, use_class_weight = use_class_weight, merge_functional = merge_functional, ngram = ngram, model = model, penalty = penalty, feature_type = feature_type, max_df = max_df, min_df = min_df, drop_ide = True, loss='loss')
                                            command = ".\models.py --input {0} --output {1} --normalize {2} --remove_stop_words {3} --use_class_weight {4} --merge_functional {5} --ngram {6} --model {7} --penalty {8} --feature_type {9} --max_df {10} --min_df {11} --drop_ide {12} --loss {13}".format('comments.tsv', 'models', normalize, remove_stop_words, use_class_weight, merge_functional, ngram, model, penalty, feature_type, max_df, min_df, True, 'loss')
                                            print("============================================================================")
                                            print(command)
                                            print("============================================================================")
                                            models.driver(args)

                                            continue

                                        for loss in loss_choices:
                                            # left hand side in Args constructor must be the same as the name of expected argument in models.py (e.g. --penalty)
                                            args = Args(input = 'comments.tsv', output='models',
                                                normalize=normalize, remove_stop_words = remove_stop_words, use_class_weight = use_class_weight, merge_functional = merge_functional, ngram = ngram, model = model, penalty = penalty, feature_type = feature_type, max_df = max_df, min_df = min_df, drop_ide = True, loss=loss)
                                            command = ".\models.py --input {0} --output {1} --normalize {2} --remove_stop_words {3} --use_class_weight {4} --merge_functional {5} --ngram {6} --model {7} --penalty {8} --feature_type {9} --max_df {10} --min_df {11} --drop_ide {12} --loss {13}".format('comments.tsv', 'models', normalize, remove_stop_words, use_class_weight, merge_functional, ngram, model, penalty, feature_type, max_df, min_df, True, loss)
                                            print("============================================================================")
                                            print(command)
                                            print("============================================================================")
                                            models.driver(args)
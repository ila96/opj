import pandas as pd
import warnings
import pickle
import matplotlib.pyplot as plt
from collections import Counter

def preprocess_comments():
    comments = pd.read_csv("comments_before_preprocessing.tsv", sep='\t')
    comments = comments.dropna(how='any',axis=0)
    comments = comments.drop_duplicates(subset=['Comment'])
    comments.to_csv(path_or_buf="comments.tsv", sep='\t', index=False)

def number_of_unique_comments():
    comments = pd.read_csv("comments.tsv", sep='\t')
    unique_comments = comments["Comment"].unique()
    print("Number of unique comments: " + str(len(unique_comments)))

def comment_types_graph():
    comment_types_df = pd.read_csv("comments.tsv", sep='\t')
    comment_types_df = comment_types_df[["CommentType"]]
    comment_types_df["CommentType"] = comment_types_df["CommentType"].str.lower()

    comment_types_dict = {}
    for comment in comment_types_df["CommentType"]:
        if comment in comment_types_dict:
            comment_types_dict[comment] += 1
        else:
            comment_types_dict[comment] = 1
    print(comment_types_dict)
    percentage_comment_types_dict = comment_types_dict
    for key in percentage_comment_types_dict:
        percentage_comment_types_dict[key] = round(percentage_comment_types_dict[key]/len(comment_types_df),4)
    print(percentage_comment_types_dict)
    comments_counts = Counter(percentage_comment_types_dict)
    x, y = zip(*comments_counts.items())
    left = [1, 2, 3, 4, 5, 6, 7, 8]
    plt.bar(left, y, tick_label = x, width =0.6)

    # naming the x-axis
    plt.xlabel("Comment type")
    # naming the y-axis
    plt.ylabel("Percentage")
    # plot title
    plt.title("Comment type distribution")

    # function to show the plot 
    plt.show()

def merge_comment_types():
    # read tsv files
    comments_mk = pd.read_csv("comments_mk.tsv", sep='\t')
    comments_nm = pd.read_csv("comments_nm.tsv", sep='\t')
    comments_sb = pd.read_csv("comments_sb.tsv", sep='\t')
    comments_ni = pd.read_csv("comments_ni.tsv", sep='\t')

    # get comments from rows 3901-4300
    comments_mk_3901_4300 =  comments_mk.iloc[3901:4301]
    comments_nm_3901_4300 =  comments_nm.iloc[3901:4301]
    comments_sb_3901_4300 =  comments_sb.iloc[3901:4301]
    comments_ni_3901_4300 =  comments_ni.iloc[3901:4301]
    
    # take only CommentID and CommentType columns
    comments_mk_3901_4300 = comments_mk_3901_4300[["CommentID", "CommentType"]]
    comments_nm_3901_4300 = comments_nm_3901_4300[["CommentID", "CommentType"]]
    comments_sb_3901_4300 = comments_sb_3901_4300[["CommentID", "CommentType"]]
    comments_ni_3901_4300 = comments_ni_3901_4300[["CommentID", "CommentType"]]

    # make all comment types lowercase
    comments_mk_3901_4300["CommentType"] = comments_mk_3901_4300["CommentType"].str.lower()
    comments_nm_3901_4300["CommentType"] = comments_nm_3901_4300["CommentType"].str.lower()
    comments_sb_3901_4300["CommentType"] = comments_sb_3901_4300["CommentType"].str.lower()
    comments_ni_3901_4300["CommentType"] = comments_ni_3901_4300["CommentType"].str.lower()

    # merge DataFrames
    comments_mk_3901_4300 = comments_mk_3901_4300.rename(columns={"CommentType": "CommentType_mk"})
    comments_mk_3901_4300["CommentType_nm"] = comments_nm_3901_4300[["CommentType"]]
    comments_mk_3901_4300["CommentType_sb"] = comments_sb_3901_4300[["CommentType"]]
    comments_mk_3901_4300["CommentType_ni"] = comments_ni_3901_4300[["CommentType"]]

    # delete rows with NaN
    comments_mk_3901_4300 = comments_mk_3901_4300.dropna(how='any',axis=0)

    # write DataFrame to tsv file
    comments_mk_3901_4300.to_csv("correlation.tsv", sep='\t', index=False)

    return comments_mk_3901_4300

def calculate_percentage_correlation(df):
    total = len(df.index)
    mk_nm = 0
    mk_sb = 0
    mk_ni = 0
    nm_sb = 0
    nm_ni = 0
    sb_ni = 0
    for index, row in df.iterrows():
        if row["CommentType_mk"] == row["CommentType_nm"]:
            mk_nm += 1
        if row["CommentType_mk"] == row["CommentType_sb"]:
            mk_sb += 1
        if row["CommentType_mk"] == row["CommentType_ni"]:
            mk_ni += 1
        if row["CommentType_nm"] == row["CommentType_sb"]:
            nm_sb += 1
        if row["CommentType_nm"] == row["CommentType_ni"]:
            nm_ni += 1
        if row["CommentType_sb"] == row["CommentType_ni"]:
            sb_ni += 1

    avg = (mk_nm+mk_sb+mk_ni+nm_sb+nm_ni+sb_ni)/(6*total)
    print("M.K. - N.M. percentage correlation: " + str(mk_nm/total))
    print("M.K. - S.B. percentage correlation: " + str(mk_sb/total))
    print("M.K. - N.I. percentage correlation: " + str(mk_ni/total))
    print("N.M. - S.B. percentage correlation: " + str(nm_sb/total))
    print("N.M. - N.I. percentage correlation: " + str(nm_ni/total))
    print("S.B. - N.I. percentage correlation: " + str(sb_ni/total))
    print("Average percentage correlation: " + str(avg))
    
def calculate_correlations(correlation_df):
    correlation_all = correlation_df[["CommentType_mk", "CommentType_nm", "CommentType_sb", "CommentType_ni"]]
    warnings.simplefilter("ignore")
    correlation_all["CommentType_mk"] = correlation_all["CommentType_mk"].astype('category').cat.codes
    correlation_all["CommentType_nm"] = correlation_all["CommentType_nm"].astype('category').cat.codes
    correlation_all["CommentType_sb"] = correlation_all["CommentType_sb"].astype('category').cat.codes
    correlation_all["CommentType_ni"] = correlation_all["CommentType_ni"].astype('category').cat.codes
    warnings.simplefilter("default")
    corr = correlation_all.corr()
    print("Pearson correlation matrix:")
    print(corr)
    corr_list = corr[["CommentType_mk", "CommentType_nm", "CommentType_sb", "CommentType_ni"]].values.tolist()
    avg_degree = (corr_list[0][1] + corr_list[0][2] + corr_list[0][3] + corr_list[1][2] + corr_list[1][3] + corr_list[2][3]) / 6
    print("Average Pearson correlation degree:")
    print(avg_degree)
    calculate_percentage_correlation(correlation_all)


# preprocess_comments()
number_of_unique_comments()
comment_types_graph()
correlation_df = merge_comment_types()
calculate_correlations(correlation_df)